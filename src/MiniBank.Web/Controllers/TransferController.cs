using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiniBank.Core.Domains.Transfers.Interfaces;
using MiniBank.Web.Dto;

namespace MiniBank.Web.Controllers;

[Authorize]
[ApiController]
[Route("/api/[controller]")]
public class TransferController
{
    private readonly ITransferHistoryManager _historyManager;
    private readonly ITransferService _transferService;

    public TransferController(ITransferService transferService, ITransferHistoryManager historyManager)
    {
        _transferService = transferService;
        _historyManager = historyManager;
    }

    /// <summary>
    ///     Осуществляет перевод денег между аккаунтами
    /// </summary>
    /// <param name="amount">Сумма</param>
    /// <param name="fromAccountId">Изначальный аккаунт</param>
    /// <param name="toAccountId">Конечный аккаунт</param>
    [HttpPost]
    public async Task TransferMoney(decimal amount, Guid fromAccountId, Guid toAccountId, CancellationToken token)
    {
        await _transferService.TransferMoney(amount, fromAccountId, toAccountId, token);
    }

    /// <summary>
    ///     Расчитывает комиссию при переводе между аккаунтами
    /// </summary>
    /// <param name="amount">Сумма</param>
    /// <param name="fromAccountId">Изначальный аккаунт</param>
    /// <param name="toAccountId">Конечный аккаунт</param>
    /// <returns>Комиссия</returns>
    [HttpGet("commission")]
    public async Task<decimal> CalculateCommission(decimal amount, Guid fromAccountId, Guid toAccountId,
        CancellationToken token)
    {
        return await _transferService.CalculateCommission(amount, fromAccountId, toAccountId, token);
    }

    /// <summary>
    ///     Возвращает историю перевода по Id
    /// </summary>
    /// <param name="historyId">Id истории перевода</param>
    /// <returns>Модель истории перевода</returns>
    [HttpGet("{historyId:guid}")]
    public async Task<TransferHistoryDto> GetById(Guid historyId, CancellationToken token)
    {
        var history = await _historyManager.GetById(historyId, token);
        return new TransferHistoryDto(history.Id, history.Amount, history.CurrencyCode, history.FromAccountId,
            history.ToAccountId);
    }

    /// <summary>
    ///     Возвращает список всех историй переводов по Id одного из участников
    /// </summary>
    /// <param name="userId">Id участника перевода</param>
    /// <returns>Список историй перевода</returns>
    [HttpPost("all/{userId:guid}")]
    public async Task<List<TransferHistoryDto>> GetAllByUserId(Guid userId, CancellationToken token)
    {
        var models = await _historyManager.GetAllByUserId(userId, token);
        return models
            .Select(history => new TransferHistoryDto(history.Id, history.Amount, history.CurrencyCode,
                history.FromAccountId, history.ToAccountId))
            .ToList();
    }
}