using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiniBank.Core.Domains.Users.Interfaces;
using MiniBank.Core.Domains.Users.Models;
using MiniBank.Web.Dto;

namespace MiniBank.Web.Controllers;

[Authorize]
[ApiController]
[Route("/api/[controller]")]
public class UserController : ControllerBase
{
    private readonly IUserManager _userManager;

    public UserController(IUserManager userManager)
    {
        _userManager = userManager;
    }

    /// <summary>
    ///     Возвращает пользователя по Id
    /// </summary>
    /// <param name="userId">Id пользователя</param>
    /// <returns>Модель пользователя</returns>
    [HttpGet("{userId:guid}")]
    public async Task<UserDto> GetById(Guid userId, CancellationToken token)
    {
        var user = await _userManager.GetById(userId, token);
        return new UserDto(user.Id, user.Email, user.Login);
    }

    /// <summary>
    ///     Создаёт пользователя
    /// </summary>
    /// <param name="model">Базовая информация о пользователе</param>
    /// <returns>Id созднанного пользователя</returns>
    [HttpPost]
    public async Task<Guid> Create(UserRegistration model, CancellationToken token)
    {
        var user = new User(Guid.Empty, model.Login, model.Email);
        return await _userManager.Create(user, model.Password, token);
    }

    /// <summary>
    ///     Обновляет данные о пользователе
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <param name="newUser">Обновлённые данные</param>
    [HttpPut("{id:guid}")]
    public async Task Update(Guid id, UserDto newUser, CancellationToken token)
    {
        var userModel = new User(id, newUser.Login, newUser.Email);
        await _userManager.Update(userModel, token);
    }

    /// <summary>
    ///     Удаляет пользователя
    /// </summary>
    /// <param name="id">Id пользователя</param>
    [HttpDelete("{id:guid}")]
    public async Task Delete(Guid id, CancellationToken token)
    {
        await _userManager.DeleteById(id, token);
    }
}