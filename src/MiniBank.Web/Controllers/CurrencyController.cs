using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiniBank.Core.Domains.Currencies.Interfaces;

namespace MiniBank.Web.Controllers;

[Authorize]
[ApiController]
[Route("/api/[controller]")]
public class CurrencyController : Controller
{
    private readonly ICurrencyConverter _converter;

    public CurrencyController(ICurrencyConverter converter)
    {
        _converter = converter;
    }

    /// <summary>
    ///     Переводит сумму из одной валюты в другую
    /// </summary>
    /// <param name="amount">Сумма</param>
    /// <param name="fromCurrency">Изначальная валюта</param>
    /// <param name="toCurrency">Конечная валюта</param>
    /// <returns>Сумма в конечной валюте</returns>
    [HttpGet]
    public async Task<decimal> ConvertValue(decimal amount, string fromCurrency, string toCurrency,
        CancellationToken token)
    {
        var currency = await _converter.ConvertTo(amount, fromCurrency, toCurrency, token);
        return currency.Value;
    }
}