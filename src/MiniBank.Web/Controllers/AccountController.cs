using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Web.Dto;

namespace MiniBank.Web.Controllers;

[Authorize]
[ApiController]
[Route("/api/[controller]")]
public class AccountController : ControllerBase
{
    private readonly IAccountManager _accountManager;

    public AccountController(IAccountManager accountManager)
    {
        _accountManager = accountManager;
    }

    /// <summary>
    ///     Возвращает аккаунт по Id
    /// </summary>
    /// <param name="accountId">Id аккаунта</param>
    /// <returns>Модель аккаунта</returns>
    [HttpGet("{accountId:guid}")]
    public async Task<AccountDto> GetById(Guid accountId, CancellationToken token)
    {
        var account = await _accountManager.GetById(accountId, token);
        return new AccountDto(account.Id, account.UserId, account.Amount, account.CurrencyCode, account.IsActive,
            account.OpeningDate.ToString(), account.ClosingDate.ToString());
    }

    /// <summary>
    ///     Возвращает все аккаунты по Id владельца
    /// </summary>
    /// <param name="userId">Id пользователя</param>
    /// <returns>Список аккаунтов</returns>
    [HttpGet("all/{userId:guid}")]
    public async Task<List<AccountDto>> GetAllByUserId(Guid userId, CancellationToken token)
    {
        var accounts = await _accountManager.GetAllByUserId(userId, token);
        return accounts.Select(account => new AccountDto(account.Id, account.UserId, account.Amount,
                account.CurrencyCode, account.IsActive, account.OpeningDate.ToString(), account.ClosingDate.ToString()))
            .ToList();
    }

    /// <summary>
    ///     Создаёт аккаунт
    /// </summary>
    /// <param name="userId">Id владельца</param>
    /// <param name="currencyCode">Код валюты аккаунта</param>
    /// <returns>Id созданного аккаунта</returns>
    [HttpPost]
    public async Task<Guid> Create(Guid userId, string currencyCode, CancellationToken token)
    {
        return await _accountManager.Create(userId, currencyCode, token);
    }

    /// <summary>
    ///     Закрывает аккаунт
    /// </summary>
    /// <param name="accountId">Id аккаунта</param>
    [HttpPatch("{accountId:guid}/close")]
    public async Task CloseById(Guid accountId, CancellationToken token)
    {
        await _accountManager.CloseById(accountId, token);
    }

    /// <summary>
    ///     Добавляет n-ю сумму на аккаунт
    /// </summary>
    /// <param name="accountId">Id аккаунта</param>
    /// <param name="amount">Сумма</param>
    [HttpPost("{accountId:guid}")]
    public async Task AddMoneyById(Guid accountId, decimal amount, CancellationToken token)
    {
        await _accountManager.AddMoneyById(accountId, amount, token);
    }
}