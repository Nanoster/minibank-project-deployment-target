using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace MiniBank.Web.Configurations;

public static class AuthConfig
{
    public static void SetUp(JwtBearerOptions options, IConfiguration config)
    {
        options.Audience = "api";
        options.Authority = config.GetConnectionString("Authorization");
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateLifetime = false,
            ValidateAudience = false
        };
    }
}