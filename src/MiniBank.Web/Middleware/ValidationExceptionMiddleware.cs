using MiniBank.Core.Exceptions;

namespace MiniBank.Web.Middleware;

public class ValidationExceptionMiddleware
{
    private readonly RequestDelegate _next;

    public ValidationExceptionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (ValidationException e)
        {
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            await context.Response.WriteAsync(e.MessageForUser);
        }
        catch (FluentValidation.ValidationException e)
        {
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            var errors = e.Errors.Select(error => $"{error.PropertyName}: {error.ErrorMessage}");
            var result = string.Join(Environment.NewLine, errors);
            await context.Response.WriteAsync(result);
        }
    }
}