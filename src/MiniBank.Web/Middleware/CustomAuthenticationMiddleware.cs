using System.Text;
using System.Text.RegularExpressions;

namespace MiniBank.Web.Middleware;

public class CustomAuthenticationMiddleware
{
    private readonly RequestDelegate _next;
    
    public CustomAuthenticationMiddleware(RequestDelegate next)
    {
        _next = next;
    }
    
    public async Task InvokeAsync(HttpContext context)
    {
        const string expPattern = @"""exp""\s*:\s*\d+,";

        if (context.Request.Headers.Authorization.Count != 1) await _next(context);
        var token = context.Request.Headers.Authorization[0].Split(' ')[1].Split('.');
        var body = token[1];
        body = Encoding.UTF8.GetString(Convert.FromBase64String(body));
        
        var expStr = Regex.Match(body, expPattern).Value;
        var exp = int.Parse(Regex.Match(expStr, @"\d+").Value);
        
        var tokenTimeLife = DateTime.UnixEpoch.AddSeconds(exp);
        if (tokenTimeLife >= DateTime.UtcNow) await _next(context);
        else context.Response.StatusCode = StatusCodes.Status403Forbidden;
    }
}