namespace MiniBank.Web.Dto;

public record TransferHistoryDto(
    Guid Id,
    decimal Amount,
    string CurrencyCode,
    Guid FromAccountId,
    Guid ToAccountId
);