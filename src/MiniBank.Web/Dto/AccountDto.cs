namespace MiniBank.Web.Dto;

public record AccountDto(
    Guid Id,
    Guid UserId,
    decimal Amount,
    string CurrencyCode,
    bool IsActive,
    string OpeningDate,
    string ClosingDate
);