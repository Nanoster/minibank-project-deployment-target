namespace MiniBank.Web.Dto;

public record UserRegistration(string Login, string Email, string Password);