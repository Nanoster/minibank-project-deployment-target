namespace MiniBank.Web.Dto;

public record UserDto(Guid Id, string Email, string Login);