using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Accounts.Models;
using MiniBank.Core.Domains.Currencies.Interfaces;
using MiniBank.Core.Domains.Currencies.Models;
using MiniBank.Core.Domains.Transfers.Interfaces;
using MiniBank.Core.Domains.Transfers.Services;
using MiniBank.Core.Exceptions;
using MiniBank.Core.UnitsOfWork;
using Moq;
using Xunit;

namespace MiniBank.Core.Tests;

public class TransferServiceTests
{
    private const decimal CommissionAmount = 0.2m;
    
    private readonly Mock<IAccountRepository> _accountRepositoryMock;
    private readonly Mock<ITransferHistoryRepository> _transferHistoryRepositoryMock;
    private readonly Mock<ICurrencyConverter> _currencyConverterMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;

    
    public TransferServiceTests()
    {
        _accountRepositoryMock = new Mock<IAccountRepository>();
        _transferHistoryRepositoryMock = new Mock<ITransferHistoryRepository>();
        _currencyConverterMock = new Mock<ICurrencyConverter>();
        _unitOfWorkMock = new Mock<IUnitOfWork>();
        
        _currencyConverterMock
            .Setup(converter => 
                converter.ConvertTo(It.IsAny<decimal>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .Returns<decimal, string, string, CancellationToken>((amount, _, _, _) => 
                Task.FromResult(new CurrencyUnit(amount, new CurrencyInfo(string.Empty, string.Empty, 0))));
    }

    [Fact] public async void CalculateCommission_WithInvalidAmount_ShouldThrowValidationException()
    {
        //Arrange
        var amount = -15;
        var service = GetTransferService();
        
        //Assert
        await Assert.ThrowsAsync<ValidationException>(() => service.CalculateCommission(amount, default, default));
    }

    [Fact]
    public async void CalculateCommission_WithOneUser_ShouldCalculateCommission()
    {
        //Arrange
        var fromId = Guid.NewGuid();
        var toId = Guid.NewGuid();
        var userId = Guid.NewGuid();
        var amount = 20m;
        var accounts = new Dictionary<Guid, Account>
        {
            {fromId, new Account(fromId, userId, 0, string.Empty, true, default, default)},
            {toId, new Account(toId, userId, 0, string.Empty, true, default, default)}
        };
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((accountId, _) => Task.FromResult(accounts[accountId]));
        var service = GetTransferService();

        //Act
        var commission = await service.CalculateCommission(amount, fromId, toId);
        
        //Assert
        Assert.Equal(0, commission);
    }


    [Fact]
    public async void CalculateCommission_WithDifferentUsers_ShouldCalculateCommission()
    {
        //Arrange
        var fromId = Guid.NewGuid();
        var toId = Guid.NewGuid();
        var fromUserId = Guid.NewGuid();
        var toUserId = Guid.NewGuid();
        var amount = 20;
        var accounts = new Dictionary<Guid, Account>
        {
            {fromId, new Account(fromId, fromUserId, 0, string.Empty, true, default, default)},
            {toId, new Account(toId, toUserId, 0, string.Empty, true, default, default)}
        };
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((accountId, _) => Task.FromResult(accounts[accountId]));
        var service = GetTransferService();

        //Act
        var commission = await service.CalculateCommission(amount, fromId, toId);

        //Assert
        Assert.Equal(amount * CommissionAmount, commission);
    }

    [Theory]
    [InlineData(0), InlineData(-10)]
    public async void TransferMoney_WithInvalidAmount_ShouldThrowValidationException(decimal amount)
    {
        //Arrange
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns(Task.FromResult(new Account(default, default, 0, string.Empty, true, default, default)));
        var service = GetTransferService();

        //Assert
        await Assert.ThrowsAsync<ValidationException>(() => service.TransferMoney(amount, default, default));
    }

    [Theory]
    [InlineData(true), InlineData(false)]
    public async void TransferMoney_WithInactiveAccount_ShouldThrowValidationException(bool isFromAcc)
    {
        //Arrange
        var fromId = Guid.NewGuid();
        var toId = Guid.NewGuid();
        var userId = Guid.NewGuid();
        var amount = 20;
        var accounts = new Dictionary<Guid, Account>
        {
            {fromId, new Account(fromId, userId, 0, string.Empty, !isFromAcc, default, default)},
            {toId, new Account(toId, userId, 0, string.Empty, isFromAcc, default, default)}
        };
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((accountId, _) => Task.FromResult(accounts[accountId]));
        var service = GetTransferService();
        
        //Assert
        await Assert.ThrowsAsync<ValidationException>(() => service.TransferMoney(amount, fromId, toId));
    }

    [Fact]
    public async void TransferMoney_WithOneUser_ShouldTransferMoney()
    {
        //Arrange
        var fromId = Guid.NewGuid();
        var toId = Guid.NewGuid();
        var userId = Guid.NewGuid();
        var amount = 20;
        var accounts = new Dictionary<Guid, Account>
        {
            {fromId, new Account(fromId, userId, 20, string.Empty, true, default, default)},
            {toId, new Account(toId, userId, 0, string.Empty, true, default, default)}
        };
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((accountId, _) => Task.FromResult(accounts[accountId]));
        var service = GetTransferService();
        
        //Act
        await service.TransferMoney(amount, fromId, toId);

        //Assert
        VerifyUnitOfWork();
        Assert.Equal(0, accounts[fromId].Amount);
        Assert.Equal(20, accounts[toId].Amount);
    }
    
    [Fact]
    public async void TransferMoney_WithDifferentUsers_ShouldTransferMoney()
    {
        //Arrange
        var fromId = Guid.NewGuid();
        var toId = Guid.NewGuid();
        var fromUserId = Guid.NewGuid();
        var toUserId = Guid.NewGuid();
        var amount = 20;
        var accounts = new Dictionary<Guid, Account>
        {
            {fromId, new Account(fromId, fromUserId, 20, string.Empty, true, default, default)},
            {toId, new Account(toId, toUserId, 0, string.Empty, true, default, default)}
        };
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((accountId, _) => Task.FromResult(accounts[accountId]));
        
        var service = GetTransferService();
        
        //Act
        await service.TransferMoney(amount, fromId, toId);

        //Assert
        VerifyUnitOfWork();
        Assert.Equal(0, accounts[fromId].Amount);
        Assert.Equal(amount - amount * CommissionAmount, accounts[toId].Amount);
    }

    private ITransferService GetTransferService()
    {
        return new TransferService(
            _accountRepositoryMock.Object,
            _transferHistoryRepositoryMock.Object,
            _currencyConverterMock.Object,
            _unitOfWorkMock.Object);
    }

    private void VerifyUnitOfWork() => _unitOfWorkMock.Verify(work => work.SaveChanges(It.IsAny<CancellationToken>()), Times.Once);
}