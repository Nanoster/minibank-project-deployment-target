using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.Extensions.Localization;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Accounts.Models;
using MiniBank.Core.Domains.Accounts.Services;
using MiniBank.Core.Domains.Accounts.Validators;
using MiniBank.Core.Domains.Users.Interfaces;
using MiniBank.Core.Resources;
using MiniBank.Core.Resources.Keys;
using MiniBank.Core.UnitsOfWork;
using Moq;
using Xunit;

namespace MiniBank.Core.Tests;

public class AccountManagerTests
{
    private readonly Mock<IAccountRepository> _accountRepositoryMock;
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IStringLocalizer<ValidationErrors>> _stringLocalizerMock;

    public AccountManagerTests()
    {
        _accountRepositoryMock = new Mock<IAccountRepository>();
        _userRepositoryMock = new Mock<IUserRepository>();
        _unitOfWorkMock = new Mock<IUnitOfWork>();
        _stringLocalizerMock = new Mock<IStringLocalizer<ValidationErrors>>();

        _stringLocalizerMock.Setup(localizer => localizer[It.IsAny<string>()])
            .Returns(new LocalizedString("name", "value"));
    }

    private IAccountManager GetAccountManager()
    {
        return new AccountManager(
            _accountRepositoryMock.Object,
            _userRepositoryMock.Object,
            _unitOfWorkMock.Object,
            new AccountCurrencyValidator(_stringLocalizerMock.Object));
    }

    [Fact]
    public async void GetAccount_WithValidData_ShouldGetAccount()
    {
        //Arrange
        var id = Guid.NewGuid();
        var accounts = new Dictionary<Guid, Account>
            {{id, new Account(id, default, 0, string.Empty, true, default, default)}};
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((accountId, _) => Task.FromResult(accounts[accountId]));
        var service = GetAccountManager();

        //Act
        var account = await service.GetById(id);

        //Assert
        Assert.Same(accounts[id], account);
    }

    [Fact]
    public async void GetAllAccounts_WithValidData_ShouldGetAllAccounts()
    {
        //Arrange
        var id = Guid.NewGuid();
        var accounts = new List<Account>
        {
            new(default, id, 0, string.Empty, true, default, default),
            new(default, id, 0, string.Empty, true, default, default)
        };
        _accountRepositoryMock
            .Setup(repository => repository.GetByUserId(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((_, _) =>
                Task.FromResult(accounts.Where(account => account.UserId == id)));
        var service = GetAccountManager();

        //Act
        var result = await service.GetAllByUserId(id);

        //Assert
        Assert.Equal(accounts, result);
    }

    [Theory]
    [MemberData(nameof(GetInvalidUserIds))]
    public async void AddAccount_WithInvalidUserId_ShouldThrowValidationException(Guid id)
    {
        //Arrange
        _userRepositoryMock.Setup(repository => repository.Exists(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns(Task.FromResult(false));
        var service = GetAccountManager();
        var curCode = GetValidCurrencyCode();

        //Act
        await Assert.ThrowsAsync<Exceptions.ValidationException>(() => service.Create(id, curCode));
    }

    [Theory]
    [MemberData(nameof(GetInvalidCurrencyCodes))]
    public async void AddAccount_WithInvalidCurrencyCode_ShouldThrowValidationException(string curCode)
    {
        //Arrange
        var service = GetAccountManager();
        var id = Guid.NewGuid();

        //Act
        await Assert.ThrowsAsync<ValidationException>(() => service.Create(id, curCode));
    }

    [Fact]
    public async void AddAccount_WithValidData_ShouldSetIdAndCreateAccount()
    {
        //Arrange
        var accounts = new List<Account>();
        var userId = Guid.NewGuid();
        var curCode = GetValidCurrencyCode();
        var expectedId = Guid.NewGuid();
        _accountRepositoryMock.Setup(repository => repository.Create(It.IsAny<Guid>(), It.IsAny<string>()))
            .Callback<Guid, string>((guid, _) =>
                accounts.Add(new Account(default, guid, 0, curCode, true, default, default)))
            .Returns(expectedId);
        _userRepositoryMock.Setup(repository => repository.Exists(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns(Task.FromResult(true));
        var service = GetAccountManager();

        //Act
        var id = await service.Create(userId, curCode);

        //Assert
        VerifyUnitOfWork();
        Assert.NotEmpty(accounts);
        Assert.Equal(expectedId, id);
        Assert.Equal(userId, accounts[0].UserId);
        Assert.Equal(curCode, accounts[0].CurrencyCode);
    }

    [Fact]
    public async void AddMoney_WithInvalidAmount_ShouldThrowValidationException()
    {
        //Arrange
        var id = Guid.NewGuid();
        var amount = -10;
        var service = GetAccountManager();

        //Assert
        await Assert.ThrowsAsync<Exceptions.ValidationException>(() => service.AddMoneyById(id, amount));
    }

    [Fact]
    public async void AddMoney_WithInactiveAccount_ShouldThrowValidationException()
    {
        //Arrange
        var id = Guid.NewGuid();
        var amount = 1;
        var accounts = new Dictionary<Guid, Account>
            {{id, new Account(id, default, 0, string.Empty, false, default, default)}};
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((_, _) => Task.FromResult(accounts[id]));
        var service = GetAccountManager();

        //Assert
        await Assert.ThrowsAsync<Exceptions.ValidationException>(() => service.AddMoneyById(id, amount));
    }

    [Fact]
    public async void AddMoney_WithValidData_ShouldAddMoneyOnAccount()
    {
        //Arrange
        var id = Guid.NewGuid();
        var accounts = new Dictionary<Guid, Account>
            {{id, new Account(id, default, 0, string.Empty, true, default, default)}};
        const int amount = 5;
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((accountId, _) => Task.FromResult(accounts[accountId]));
        _accountRepositoryMock
            .Setup(repository => repository.Update(It.IsAny<Account>(), It.IsAny<CancellationToken>()))
            .Callback<Account, CancellationToken>((account, _) => accounts[account.Id] = account);
        var service = GetAccountManager();

        //Act
        await service.AddMoneyById(id, amount);

        //Assert
        VerifyUnitOfWork();
        Assert.Equal(amount, accounts[id].Amount);
    }

    [Fact]
    public async void CloseAccount_WithNonzeroAmount_ShouldThrowValidationException()
    {
        //Arrange
        var id = Guid.NewGuid();
        var accounts = new Dictionary<Guid, Account> { {id, new Account(id, default, 5, string.Empty, true, default, default)} };
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid,CancellationToken>((accountId,_) => Task.FromResult(accounts[accountId]));
        var service = GetAccountManager();
        
        //Assert
        await Assert.ThrowsAsync<Exceptions.ValidationException>(() => service.CloseById(id));
    }
    
    [Fact]
    public async void CloseAccount_WithInactiveState_ShouldThrowValidationException()
    {
        //Arrange
        var id = Guid.NewGuid();
        var accounts = new Dictionary<Guid, Account> { {id, new Account(id, default, 0, string.Empty, false, default, default)} };
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid,CancellationToken>((accountId,_) => Task.FromResult(accounts[accountId]));
        var service = GetAccountManager();
        
        //Assert
        await Assert.ThrowsAsync<Exceptions.ValidationException>(() => service.CloseById(id));
    }

    [Fact]
    public async void CloseAccount_WithValidData_ShouldSetIsActiveToFalseAndSetClosingDateAndUpdateAccount()
    {
        //Arrange
        var id = Guid.NewGuid();
        var accounts = new Dictionary<Guid, Account> { {id, new Account(id, default, 0, string.Empty, true, default, default)} };
        _accountRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid,CancellationToken>((accountId,_) => Task.FromResult(accounts[accountId]));
        var service = GetAccountManager();
        
        //Act
        await service.CloseById(id);

        //Assert
        VerifyUnitOfWork();
        Assert.False(accounts[id].IsActive);
        Assert.NotEqual(DateOnly.MinValue, accounts[id].ClosingDate);
    }

    public static object[][] GetInvalidUserIds() => new[]
    {
        new object[] { null! },
        new object[] { Guid.Empty }
    };

    public static object[][] GetInvalidCurrencyCodes() => new[]
    {
        new object[] { null! },
        new object[] { string.Empty },
        new object[] { "ABCSDF" }
    };
    
    private void VerifyUnitOfWork() => _unitOfWorkMock.Verify(work => work.SaveChanges(It.IsAny<CancellationToken>()), Times.Once);
    private string GetValidCurrencyCode() => "EUR";
}