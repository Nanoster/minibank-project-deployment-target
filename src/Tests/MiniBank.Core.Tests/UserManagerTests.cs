using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.Extensions.Localization;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Users.Interfaces;
using MiniBank.Core.Domains.Users.Models;
using MiniBank.Core.Domains.Users.Services;
using MiniBank.Core.Domains.Users.Validators;
using MiniBank.Core.Resources;
using MiniBank.Core.Resources.Keys;
using MiniBank.Core.UnitsOfWork;
using Moq;
using Xunit;

namespace MiniBank.Core.Tests;

public class UserManagerTests
{
    private readonly Mock<IAccountRepository> _accountRepositoryMock;
    private readonly Mock<IStringLocalizer<ValidationErrors>> _stringLocalizerMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IUserRepository> _userRepositoryMock;

    public UserManagerTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
        _accountRepositoryMock = new Mock<IAccountRepository>();
        _unitOfWorkMock = new Mock<IUnitOfWork>();
        _stringLocalizerMock = new Mock<IStringLocalizer<ValidationErrors>>();

        _stringLocalizerMock
            .Setup(localizer => localizer[It.IsAny<string>()])
            .Returns(new LocalizedString("name", "value"));
    }

    private IUserManager GetUserManager()
    {
        return new UserManager(
            _userRepositoryMock.Object,
            _accountRepositoryMock.Object,
            _unitOfWorkMock.Object,
            new PasswordValidator(_stringLocalizerMock.Object),
            new UserValidator(_stringLocalizerMock.Object));
    }

    [Theory]
    [MemberData(nameof(GetInvalidLogins))]
    public async void AddUser_WithInvalidLogin_ShouldThrowValidationException(string? login)
    {
        //Arrange
        var service = GetUserManager();
        var user = new User(default, login!, GetValidEmail());
        var password = GetValidPassword();

        //Assert
        await Assert.ThrowsAsync<ValidationException>(() => service.Create(user, password));
    }

    [Theory]
    [MemberData(nameof(GetInvalidPasswords))]
    public async void AddUser_WithInvalidPassword_ShouldThrowValidationException(string? password)
    {
        //Arrange
        var service = GetUserManager();
        var user = new User(default, GetValidLogin(), GetValidEmail());

        //Assert
        await Assert.ThrowsAsync<ValidationException>(() => service.Create(user, password!));
    }

    [Theory]
    [MemberData(nameof(GetInvalidEmails))]
    public async void AddUser_WithInvalidEmail_ShouldThrowValidationException(string? email)
    {
        //Arrange
        var service = GetUserManager();
        var user = new User(default, GetValidLogin(), email!);
        var password = GetValidPassword();

        //Assert
        await Assert.ThrowsAsync<ValidationException>(() => service.Create(user, password));
    }

    [Fact]
    public async void AddUser_WithValidData_ShouldSetIdAndAddUser()
    {
        //Arrange
        var users = new List<User>();
        _userRepositoryMock
            .Setup(repository => repository.Create(
                It.IsAny<User>(),
                It.IsAny<string>(),
                It.IsAny<CancellationToken>()))
            .Returns(Task.FromResult(Guid.NewGuid()))
            .Callback<User, string, CancellationToken>((user, _, _) => users.Add(user));
        var service = GetUserManager();
        var user = new User(default, GetValidLogin(), GetValidEmail());
        var password = GetValidPassword();

        //Act
        var id = await service.Create(user, password);

        //Assert
        VerifyUnitOfWork();
        Assert.NotEqual(Guid.Empty, id);
        Assert.NotEmpty(users);
    }

    [Theory]
    [MemberData(nameof(GetInvalidLogins))]
    public async void UpdateUser_WithInvalidLogin_ShouldThrowValidationException(string? login)
    {
        //Arrange
        var service = GetUserManager();
        var user = new User(default, login!, GetValidEmail());

        //Assert
        await Assert.ThrowsAsync<ValidationException>(() => service.Update(user));
    }

    [Theory]
    [MemberData(nameof(GetInvalidEmails))]
    public async void UpdateUser_WithInvalidEmail_ShouldThrowValidationException(string? email)
    {
        //Arrange
        var service = GetUserManager();
        var user = new User(default, GetValidLogin(), email!);

        //Assert
        await Assert.ThrowsAsync<ValidationException>(() => service.Update(user));
    }

    [Fact]
    public async void UpdateUser_WithValidData_ShouldUpdateModel()
    {
        //Arrange
        var originUser = new User(default, string.Empty, string.Empty);
        var expectedUser = new User(default, GetValidLogin(), GetValidEmail());
        _userRepositoryMock.Setup(repository => repository.Update(It.IsAny<User>(), It.IsAny<CancellationToken>()))
            .Callback<User, CancellationToken>((user, _) => originUser = user);
        var service = GetUserManager();

        //Act
        await service.Update(expectedUser);

        //Assert
        VerifyUnitOfWork();
        Assert.Same(expectedUser, originUser);
    }

    [Fact]
    public async void DeleteUser_WithNonexistentId_ShouldThrowValidationException()
    {
        //Arrange
        var id = Guid.Empty;
        _accountRepositoryMock.Setup(repository =>
                repository.AccountsByUserIdExist(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns(Task.FromResult(true));
        var service = GetUserManager();

        //Assert
        await Assert.ThrowsAsync<Exceptions.ValidationException>(() => service.DeleteById(id));
    }

    [Fact]
    public async void DeleteUser_WithValidData_ShouldDeleteUser()
    {
        //Arrange
        var id = Guid.NewGuid();
        var users = new Dictionary<Guid, User> {{id, new User(default, string.Empty, string.Empty)}};
        _userRepositoryMock.Setup(repository => repository.DeleteById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Callback<Guid, CancellationToken>((userId, _) => users.Remove(userId));
        var service = GetUserManager();

        //Act
        await service.DeleteById(id);

        //Assert
        VerifyUnitOfWork();
        Assert.Empty(users);
    }

    [Fact]
    public async void GetUser_WithValidData_ShouldGetUser()
    {
        //Arrange
        var id = Guid.NewGuid();
        var users = new Dictionary<Guid, User> {{id, new User(default, string.Empty, string.Empty)}};
        _userRepositoryMock.Setup(repository => repository.GetById(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .Returns<Guid, CancellationToken>((userId, _) => Task.FromResult(users[userId]));
        var service = GetUserManager();

        //Act
        var user = await service.GetById(id);

        //Assert
        Assert.Same(users[id], user);
    }

    private void VerifyUnitOfWork()
    {
        _unitOfWorkMock.Verify(work => work.SaveChanges(It.IsAny<CancellationToken>()), Times.Once);
    }

    private string GetValidPassword() => "qwertyuio1";

    private string GetValidLogin() => "qwertyuio1";

    private string GetValidEmail() => "qwewrt@mail.ru";

    private static object[][] GetInvalidPasswords() => new[]
    {
        new object[] {null!},
        new object[] {string.Empty},
        new object[] {"1asdsvsfdv"},
        new object[] {"s2"}
    };

    private static object[][] GetInvalidLogins() => new[]
    {
        new object[] {null!},
        new object[] {string.Empty},
        new object[] {"1asdsvsfdv"},
        new object[] {"s2"}
    };

    public static object[][] GetInvalidEmails() => new[]
    {
        new object[] {null!},
        new object[] {string.Empty},
        new object[] {"sasdf.ru"}
    };
}