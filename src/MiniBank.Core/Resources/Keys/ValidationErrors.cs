namespace MiniBank.Core.Resources.Keys;

public class ValidationErrors
{
    public const string EmailError = "EmailError";
    public const string PasswordError = "PasswordError";
    public const string LoginError = "LoginError";
    public const string GreaterThan = "GreaterThan";
    public const string InError = "InError";
}