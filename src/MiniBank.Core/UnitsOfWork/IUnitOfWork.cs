namespace MiniBank.Core.UnitsOfWork;

public interface IUnitOfWork
{
    public Task<int> SaveChanges(CancellationToken token = default);
}