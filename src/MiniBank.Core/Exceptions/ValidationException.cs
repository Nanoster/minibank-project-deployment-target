using System.Runtime.Serialization;

namespace MiniBank.Core.Exceptions;

public class ValidationException : Exception
{
    protected ValidationException(SerializationInfo info, StreamingContext context, string messageForUser) : base(info,
        context)
    {
        MessageForUser = messageForUser;
    }

    public ValidationException(string messageForUser)
    {
        MessageForUser = messageForUser;
    }

    public ValidationException(string? message, string messageForUser) : base(message)
    {
        MessageForUser = messageForUser;
    }

    public ValidationException(string? message, Exception? innerException, string messageForUser) : base(message,
        innerException)
    {
        MessageForUser = messageForUser;
    }

    public string MessageForUser { get; set; }
}