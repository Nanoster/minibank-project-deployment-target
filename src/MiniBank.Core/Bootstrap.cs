using System.Reflection;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Accounts.Services;
using MiniBank.Core.Domains.Currencies.Interfaces;
using MiniBank.Core.Domains.Currencies.Services;
using MiniBank.Core.Domains.Transfers.Interfaces;
using MiniBank.Core.Domains.Transfers.Services;
using MiniBank.Core.Domains.Users.Interfaces;
using MiniBank.Core.Domains.Users.Services;

namespace MiniBank.Core;

public static class Bootstrap
{
    public static IServiceCollection AddCore(this IServiceCollection services)
    {
        services.AddScoped<ICurrencyConverter, CurrencyConverter>();
        services.AddScoped<ITransferService, TransferService>();
        services.AddScoped<ITransferHistoryManager, TransferHistoryManager>();
        services.AddScoped<IAccountManager, AccountManager>();
        services.AddScoped<IUserManager, UserManager>();
        services.AddScoped<IPasswordHashProvider, PasswordHashProvider>();
        services.AddLocalization();
        services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
        return services;
    }
}