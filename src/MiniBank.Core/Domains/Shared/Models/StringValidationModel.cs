namespace MiniBank.Core.Domains.Shared.Models;

public record StringValidationModel(string Value)
{
    public static implicit operator StringValidationModel(string str) => new(str);
}