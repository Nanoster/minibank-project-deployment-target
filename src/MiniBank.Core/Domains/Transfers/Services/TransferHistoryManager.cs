using MiniBank.Core.Domains.Transfers.Interfaces;
using MiniBank.Core.Domains.Transfers.Models;

namespace MiniBank.Core.Domains.Transfers.Services;

public class TransferHistoryManager : ITransferHistoryManager
{
    private readonly ITransferHistoryRepository _historyRepository;

    public TransferHistoryManager(ITransferHistoryRepository historyRepository)
    {
        _historyRepository = historyRepository;
    }

    public async Task<TransferHistory> GetById(Guid historyId, CancellationToken token = default)
    {
        return await _historyRepository.GetById(historyId, token);
    }

    public async Task<IEnumerable<TransferHistory>> GetAllByUserId(Guid userId, CancellationToken token = default)
    {
        return await _historyRepository.GetByUserId(userId, token);
    }
}