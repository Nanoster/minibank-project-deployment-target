using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Accounts.Models;
using MiniBank.Core.Domains.Currencies.Interfaces;
using MiniBank.Core.Domains.Transfers.Interfaces;
using MiniBank.Core.Domains.Transfers.Models;
using MiniBank.Core.Exceptions;
using MiniBank.Core.UnitsOfWork;

namespace MiniBank.Core.Domains.Transfers.Services;

public class TransferService : ITransferService
{
    private readonly IAccountRepository _accountRepository;
    private readonly ICurrencyConverter _currencyConverter;
    private readonly ITransferHistoryRepository _transferHistoryRepository;
    private readonly IUnitOfWork _unitOfWork;

    public TransferService(IAccountRepository accountRepository, ITransferHistoryRepository transferHistoryRepository,
        ICurrencyConverter converter, IUnitOfWork unitOfWork)
    {
        _accountRepository = accountRepository;
        _transferHistoryRepository = transferHistoryRepository;
        _currencyConverter = converter;
        _unitOfWork = unitOfWork;
    }

    public async Task TransferMoney(decimal amount, Guid fromAccountId, Guid toAccountId,
        CancellationToken token = default)
    {
        var fromAccount = await _accountRepository.GetById(fromAccountId, token);
        var toAccount = await _accountRepository.GetById(toAccountId, token);

        if (!fromAccount.IsActive || !toAccount.IsActive)
            throw new ValidationException(
                $"One of the accounts is closed\n{fromAccount.Id}: {fromAccount.IsActive}\n{toAccount.Id}: {toAccount.IsActive}",
                "One of the accounts is closed");

        if (fromAccount.Amount < amount || amount <= 0)
            throw new ValidationException(
                $"Invalid amount\n({fromAccount.Id}: {fromAccount.Amount}\n({toAccount.Id}: {toAccount.Amount}\nAmount: {amount})",
                "Invalid amount");

        var commission = CalculateCommission(amount, fromAccount, toAccount);

        var sendAmount =
            await _currencyConverter.ConvertTo(amount - commission, fromAccount.CurrencyCode, toAccount.CurrencyCode,
                token);

        fromAccount.Amount -= amount;
        toAccount.Amount += sendAmount.Value;

        var history = new TransferHistory(Guid.Empty, amount, fromAccount.CurrencyCode, fromAccountId, toAccountId);

        _transferHistoryRepository.Create(history);
        await _accountRepository.Update(fromAccount, token);
        await _accountRepository.Update(toAccount, token);

        await _unitOfWork.SaveChanges(token);
    }

    public async Task<decimal> CalculateCommission(decimal amount, Guid fromAccountId, Guid toAccountId,
        CancellationToken token = default)
    {
        if (amount < 0) throw new ValidationException(
            $"Amount меньше 0. Amount: {amount}", 
            "Amount не может быть меньше 0");
        
        var fromAccount = await _accountRepository.GetById(fromAccountId, token);
        var toAccount = await _accountRepository.GetById(toAccountId, token);

        return CalculateCommission(amount, fromAccount, toAccount);
    }

    private decimal CalculateCommission(decimal amount, Account fromAccount, Account toAccount)
    {
        return fromAccount.UserId == toAccount.UserId ? 0 : Math.Round(amount * 0.2m, 2);
    }
}