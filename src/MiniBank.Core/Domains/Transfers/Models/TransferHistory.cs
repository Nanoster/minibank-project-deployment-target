namespace MiniBank.Core.Domains.Transfers.Models;

public class TransferHistory
{
    public TransferHistory(Guid id, decimal amount, string currencyCode, Guid fromAccountId,
        Guid toAccountId)
    {
        Id = id;
        Amount = amount;
        CurrencyCode = currencyCode;
        FromAccountId = fromAccountId;
        ToAccountId = toAccountId;
    }

    public Guid Id { get; set; }
    public decimal Amount { get; set; }
    public string CurrencyCode { get; set; }
    public Guid FromAccountId { get; set; }
    public Guid ToAccountId { get; set; }
}