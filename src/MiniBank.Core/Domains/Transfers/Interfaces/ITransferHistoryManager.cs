using MiniBank.Core.Domains.Transfers.Models;

namespace MiniBank.Core.Domains.Transfers.Interfaces;

public interface ITransferHistoryManager
{
    public Task<TransferHistory> GetById(Guid historyId, CancellationToken token = default);
    public Task<IEnumerable<TransferHistory>> GetAllByUserId(Guid userId, CancellationToken token = default);
}