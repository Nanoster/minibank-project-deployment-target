namespace MiniBank.Core.Domains.Transfers.Interfaces;

public interface ITransferService
{
    public Task TransferMoney(decimal amount, Guid fromAccountId, Guid toAccountId, CancellationToken token = default);

    public Task<decimal> CalculateCommission(decimal amount, Guid fromAccountId, Guid toAccountId,
        CancellationToken token = default);
}