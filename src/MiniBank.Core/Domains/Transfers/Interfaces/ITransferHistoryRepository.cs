using MiniBank.Core.Domains.Transfers.Models;

namespace MiniBank.Core.Domains.Transfers.Interfaces;

public interface ITransferHistoryRepository
{
    public Task<TransferHistory> GetById(Guid historyId, CancellationToken token = default);
    public Task<IEnumerable<TransferHistory>> GetByUserId(Guid userId, CancellationToken token = default);
    public Guid Create(TransferHistory history);
}