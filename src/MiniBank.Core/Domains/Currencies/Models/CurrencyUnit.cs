namespace MiniBank.Core.Domains.Currencies.Models;

public class CurrencyUnit
{
    public CurrencyUnit(decimal value, CurrencyInfo info)
    {
        Value = value;
        Info = info;
    }

    public CurrencyInfo Info { get; }
    public decimal Value { get; set; }
}