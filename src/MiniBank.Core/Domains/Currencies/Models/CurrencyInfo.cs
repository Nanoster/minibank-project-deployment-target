namespace MiniBank.Core.Domains.Currencies.Models;

public record CurrencyInfo(string Name, string CharCode, int Code);