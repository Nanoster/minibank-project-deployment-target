using MiniBank.Core.Domains.Currencies.Interfaces;
using MiniBank.Core.Domains.Currencies.Models;
using MiniBank.Core.Exceptions;

namespace MiniBank.Core.Domains.Currencies.Services;

public class CurrencyConverter : ICurrencyConverter
{
    private readonly ICurrencyRepository _repository;

    public CurrencyConverter(ICurrencyRepository repository)
    {
        _repository = repository;
    }

    public async Task<CurrencyUnit> ConvertTo(decimal amount, string fromCurrency, string toCurrency,
        CancellationToken token = default)
    {
        var exceptedInfo = await _repository.GetByCode(toCurrency, token);

        if (fromCurrency == toCurrency) return new CurrencyUnit(amount, exceptedInfo);

        var currentRate = await _repository.GetCurrencyRateByCode(fromCurrency, token);
        var exceptedRate = await _repository.GetCurrencyRateByCode(toCurrency, token);
        var result = currentRate * amount / exceptedRate;
        if (result > 0) return new CurrencyUnit(result, exceptedInfo);

        var currentInfo = await _repository.GetByCode(fromCurrency, token);
        throw new ValidationException(
            $"Invalid exchange rate:\n\r{currentInfo}: {currentRate}\n\r{exceptedInfo}: {exceptedRate}",
            "An error occurred in calculating the currency exchange rate"
        );
    }
}