using MiniBank.Core.Domains.Currencies.Models;

namespace MiniBank.Core.Domains.Currencies.Interfaces;

public interface ICurrencyRepository
{
    public Task<decimal> GetCurrencyRateByCode(string currencyCode, CancellationToken token = default);
    public Task<CurrencyInfo> GetByCode(string currencyCode, CancellationToken token = default);
}