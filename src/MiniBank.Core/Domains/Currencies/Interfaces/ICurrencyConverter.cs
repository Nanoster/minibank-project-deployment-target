using MiniBank.Core.Domains.Currencies.Models;

namespace MiniBank.Core.Domains.Currencies.Interfaces;

public interface ICurrencyConverter
{
    public Task<CurrencyUnit> ConvertTo(decimal amount, string fromCurrency, string toCurrency,
        CancellationToken token = default);
}