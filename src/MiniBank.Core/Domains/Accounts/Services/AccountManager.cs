using FluentValidation;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Accounts.Models;
using MiniBank.Core.Domains.Accounts.Validators;
using MiniBank.Core.Domains.Users.Interfaces;
using MiniBank.Core.UnitsOfWork;
using ValidationException = MiniBank.Core.Exceptions.ValidationException;

namespace MiniBank.Core.Domains.Accounts.Services;

public class AccountManager : IAccountManager
{
    private readonly AccountCurrencyValidator _accountCurrencyValidator;
    private readonly IAccountRepository _accountRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUserRepository _userRepository;

    public AccountManager(IAccountRepository accountRepository, IUserRepository userRepository, IUnitOfWork unitOfWork,
        AccountCurrencyValidator accountCurrencyValidator)
    {
        _accountRepository = accountRepository;
        _userRepository = userRepository;
        _unitOfWork = unitOfWork;
        _accountCurrencyValidator = accountCurrencyValidator;
    }

    public async Task CloseById(Guid accountId, CancellationToken token = default)
    {
        var account = await _accountRepository.GetById(accountId, token);

        if (account.Amount != 0) throw new ValidationException("There is an amount on this account");
        if (account.IsActive == false) throw new ValidationException("This account is closed already");
        
        account.IsActive = false;
        account.ClosingDate = DateOnly.FromDateTime(DateTime.UtcNow);
        await _accountRepository.Update(account, token);
        
        await _unitOfWork.SaveChanges(token);
    }

    public async Task<Guid> Create(Guid userId, string currencyCode, CancellationToken token = default)
    {
        await _accountCurrencyValidator.ValidateAndThrowAsync(currencyCode, token);
        currencyCode = currencyCode.ToUpper();

        if (!await _userRepository.Exists(userId, token))
            throw new ValidationException($"{userId} User doesn't exist");

        var guid = _accountRepository.Create(userId, currencyCode);
        await _unitOfWork.SaveChanges(token);
        return guid;
    }

    public async Task<Account> GetById(Guid accountId, CancellationToken token = default)
    {
        return await _accountRepository.GetById(accountId, token);
    }

    public async Task<IEnumerable<Account>> GetAllByUserId(Guid userId, CancellationToken token = default)
    {
        return await _accountRepository.GetByUserId(userId, token);
    }

    public async Task AddMoneyById(Guid accountId, decimal amount, CancellationToken token = default)
    {
        if (amount < 0)
            throw new ValidationException(
                $"Amount меньше нуля. Amount: {amount}",
                $"Количество не может быть меньше 0. Текущее кол-во {amount}");

        var account = await _accountRepository.GetById(accountId, token);

        if (!account.IsActive)
            throw new ValidationException(
                "Аккаунт закрыт",
                $"Аккаунт закрыт. Id: {account.Id}");
        
        account.Amount += amount;
        await _accountRepository.Update(account, token);
        await _unitOfWork.SaveChanges(token);
    }
}