namespace MiniBank.Core.Domains.Accounts.Models;

public class Account
{
    public Account(Guid id, Guid userId, decimal amount, string currencyCode, bool isActive,
        DateOnly openingDate, DateOnly closingDate)
    {
        Id = id;
        UserId = userId;
        Amount = amount;
        CurrencyCode = currencyCode;
        IsActive = isActive;
        OpeningDate = openingDate;
        ClosingDate = closingDate;
    }

    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public decimal Amount { get; set; }
    public string CurrencyCode { get; set; }
    public bool IsActive { get; set; }
    public DateOnly OpeningDate { get; set; }
    public DateOnly ClosingDate { get; set; }
}