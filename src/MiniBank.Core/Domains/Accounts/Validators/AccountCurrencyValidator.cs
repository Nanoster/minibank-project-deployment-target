using FluentValidation;
using Microsoft.Extensions.Localization;
using MiniBank.Core.Domains.Shared.Models;
using MiniBank.Core.Resources;
using MiniBank.Core.Resources.Keys;

namespace MiniBank.Core.Domains.Accounts.Validators;

public class AccountCurrencyValidator : AbstractValidator<StringValidationModel>
{
    private readonly IReadOnlyList<string> _availableCharCodes = new[] {"USD", "EUR", "RUB"};
    private readonly IStringLocalizer<ValidationErrors> _localizer;

    public AccountCurrencyValidator(IStringLocalizer<ValidationErrors> localizer)
    {
        _localizer = localizer;
        SetUpRules();
    }

    private void SetUpRules()
    {
        RuleFor(code => code.Value)
            .NotEmpty()
            .Must(code => _availableCharCodes.Contains(code?.ToUpper() ?? string.Empty))
            .WithMessage(string.Format(_localizer[ValidationErrors.InError],
                $"[{string.Join(',', _availableCharCodes)}]"))
            .WithName("Account's currency");
    }
}