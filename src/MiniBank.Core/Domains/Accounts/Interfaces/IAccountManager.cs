using MiniBank.Core.Domains.Accounts.Models;

namespace MiniBank.Core.Domains.Accounts.Interfaces;

public interface IAccountManager
{
    public Task CloseById(Guid accountId, CancellationToken token = default);
    public Task<Guid> Create(Guid userId, string currencyCode, CancellationToken token = default);
    public Task<Account> GetById(Guid accountId, CancellationToken token = default);
    public Task<IEnumerable<Account>> GetAllByUserId(Guid userId, CancellationToken token = default);
    public Task AddMoneyById(Guid accountId, decimal amount, CancellationToken token = default);
}