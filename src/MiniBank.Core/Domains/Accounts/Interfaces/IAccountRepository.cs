using MiniBank.Core.Domains.Accounts.Models;

namespace MiniBank.Core.Domains.Accounts.Interfaces;

public interface IAccountRepository
{
    public Task<Account> GetById(Guid accountId, CancellationToken token = default);
    public Task<IEnumerable<Account>> GetByUserId(Guid userId, CancellationToken token = default);
    public Guid Create(Guid userId, string currencyCode);
    public Task Update(Account account, CancellationToken token = default);
    public Task<bool> AccountsByUserIdExist(Guid userId, CancellationToken token = default);
}