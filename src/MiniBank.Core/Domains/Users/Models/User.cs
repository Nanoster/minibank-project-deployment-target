namespace MiniBank.Core.Domains.Users.Models;

public class User
{
    public User(Guid id, string login, string email)
    {
        Id = id;
        Login = login;
        Email = email;
    }

    public Guid Id { get; set; }
    public string Login { get; set; }
    public string Email { get; set; }
}