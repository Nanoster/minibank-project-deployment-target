using FluentValidation;
using Microsoft.Extensions.Localization;
using MiniBank.Core.Domains.Users.Models;
using MiniBank.Core.Resources;
using MiniBank.Core.Resources.Keys;

namespace MiniBank.Core.Domains.Users.Validators;

public class UserValidator : AbstractValidator<User>
{
    private readonly IStringLocalizer<ValidationErrors> _localizer;

    public UserValidator(IStringLocalizer<ValidationErrors> localizer)
    {
        _localizer = localizer;
        SetUpRules();
    }

    private void SetUpRules()
    {
        RuleFor(u => u.Login)
            .Length(5, 15)
            .Must(l => l is {Length: > 0} && !char.IsDigit(l[0]))
            .WithMessage(_localizer[ValidationErrors.LoginError]);

        RuleFor(u => u.Email)
            .NotEmpty()
            .EmailAddress()
            .WithMessage(_localizer[ValidationErrors.EmailError]);
    }
}