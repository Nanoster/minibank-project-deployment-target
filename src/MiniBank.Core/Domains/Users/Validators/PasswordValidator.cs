using System.Text.RegularExpressions;
using FluentValidation;
using Microsoft.Extensions.Localization;
using MiniBank.Core.Domains.Shared.Models;
using MiniBank.Core.Resources;
using MiniBank.Core.Resources.Keys;
using static System.String;

namespace MiniBank.Core.Domains.Users.Validators;

public class PasswordValidator : AbstractValidator<StringValidationModel>
{
    private readonly IStringLocalizer<ValidationErrors> _localizer;

    public PasswordValidator(IStringLocalizer<ValidationErrors> localizer)
    {
        _localizer = localizer;
        SetUpRules();
    }

    private void SetUpRules()
    {
        var pattern = new Regex(@"\w+\d+\w*", RegexOptions.Compiled);

        RuleFor(password => password.Value)
            .NotEmpty()
            .Length(7, 30)
            .Must(p => pattern.IsMatch(p ?? Empty))
            .WithMessage(_localizer[ValidationErrors.PasswordError])
            .WithName("Password");
    }
}