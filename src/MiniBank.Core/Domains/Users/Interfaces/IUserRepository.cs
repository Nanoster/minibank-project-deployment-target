using MiniBank.Core.Domains.Users.Models;

namespace MiniBank.Core.Domains.Users.Interfaces;

public interface IUserRepository
{
    public Task<User> GetById(Guid userId, CancellationToken token = default);
    public Task<Guid> Create(User user, string password, CancellationToken token = default);
    public Task Update(User user, CancellationToken token = default);
    public Task DeleteById(Guid userId, CancellationToken token = default);
    public Task<bool> Exists(Guid userId, CancellationToken token = default);
}