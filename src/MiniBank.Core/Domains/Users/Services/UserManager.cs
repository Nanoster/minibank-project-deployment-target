using FluentValidation;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Users.Interfaces;
using MiniBank.Core.Domains.Users.Models;
using MiniBank.Core.Domains.Users.Validators;
using MiniBank.Core.UnitsOfWork;
using ValidationException = MiniBank.Core.Exceptions.ValidationException;

namespace MiniBank.Core.Domains.Users.Services;

public class UserManager : IUserManager
{
    private readonly IAccountRepository _accountRepository;
    private readonly PasswordValidator _passwordValidator;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUserRepository _userRepository;
    private readonly UserValidator _userValidator;

    public UserManager(IUserRepository userRepository, IAccountRepository accountRepository, IUnitOfWork unitOfWork,
        PasswordValidator passwordValidator, UserValidator userValidator)
    {
        _userRepository = userRepository;
        _accountRepository = accountRepository;
        _unitOfWork = unitOfWork;
        _passwordValidator = passwordValidator;
        _userValidator = userValidator;
    }

    public async Task Update(User user, CancellationToken token = default)
    {
        await _userValidator.ValidateAndThrowAsync(user, token);
        await _userRepository.Update(user, token);
        await _unitOfWork.SaveChanges(token);
    }

    public async Task DeleteById(Guid userId, CancellationToken token = default)
    {
        if (!await _accountRepository.AccountsByUserIdExist(userId, token))
            await _userRepository.DeleteById(userId, token);
        else throw new ValidationException("This user has linked accounts");
        await _unitOfWork.SaveChanges(token);
    }

    public async Task<Guid> Create(User user, string password, CancellationToken token = default)
    {
        await _userValidator.ValidateAndThrowAsync(user, token);
        await _passwordValidator.ValidateAndThrowAsync(password, token);
        var guid = await _userRepository.Create(user, password, token);
        await _unitOfWork.SaveChanges(token);
        return guid;
    }

    public async Task<User> GetById(Guid userId, CancellationToken token = default)
    {
        return await _userRepository.GetById(userId, token);
    }
}