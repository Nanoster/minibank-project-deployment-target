using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Currencies.Interfaces;
using MiniBank.Core.Domains.Transfers.Interfaces;
using MiniBank.Core.Domains.Users.Interfaces;
using MiniBank.Core.UnitsOfWork;
using MiniBank.Data.Domains.Accounts.Repositories;
using MiniBank.Data.Domains.Currencies.Repositories;
using MiniBank.Data.Domains.TransferHistories.Repositories;
using MiniBank.Data.Domains.Users.Repositories;
using MiniBank.Data.UnitsOfWork;

namespace MiniBank.Data;

public static class Bootstrap
{
    public static IServiceCollection AddData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddHttpClient<ICurrencyRepository, CurrencyRepository>(client =>
            client.BaseAddress = new Uri(configuration.GetConnectionString("Currencies")));
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IAccountRepository, AccountRepository>();
        services.AddScoped<ITransferHistoryRepository, TransferHistoryRepository>();
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddDbContext<MiniBankContext>(builder =>
            builder.UseNpgsql(configuration.GetConnectionString("Database"))
                .UseSnakeCaseNamingConvention());
        return services;
    }
}