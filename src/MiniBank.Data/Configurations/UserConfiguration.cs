using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiniBank.Data.Domains.Users.DbModels;

namespace MiniBank.Data.Configurations;

public class UserConfiguration : IEntityTypeConfiguration<UserDbModel>
{
    public void Configure(EntityTypeBuilder<UserDbModel> builder)
    {
        builder.HasKey(model => model.Id);

        builder.ToTable("user");

        builder.Property(model => model.Login)
            .IsRequired();

        builder.Property(model => model.Email)
            .IsRequired();

        builder.Property(model => model.Password)
            .IsRequired();

        builder.Property(model => model.Salt)
            .IsRequired();
    }
}