using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiniBank.Data.Domains.Accounts.DbModels;
using MiniBank.Data.Domains.Users.DbModels;

namespace MiniBank.Data.Configurations;

public class AccountConfiguration : IEntityTypeConfiguration<AccountDbModel>
{
    public void Configure(EntityTypeBuilder<AccountDbModel> builder)
    {
        builder.HasKey(model => model.Id);

        builder.ToTable("account");

        builder.Property(model => model.UserId)
            .IsRequired();

        builder.Property(model => model.Amount)
            .IsRequired();

        builder.Property(model => model.IsActive)
            .IsRequired();

        builder.Property(model => model.CurrencyCode)
            .IsRequired();

        builder.Property(model => model.OpeningDate)
            .IsRequired();

        builder.HasOne<UserDbModel>()
            .WithMany()
            .HasForeignKey(model => model.UserId);
    }
}