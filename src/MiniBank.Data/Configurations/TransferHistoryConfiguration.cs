using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiniBank.Data.Domains.Accounts.DbModels;
using MiniBank.Data.Domains.TransferHistories.DbModels;

namespace MiniBank.Data.Configurations;

public class TransferHistoryConfiguration : IEntityTypeConfiguration<TransferHistoryDbModel>
{
    public void Configure(EntityTypeBuilder<TransferHistoryDbModel> builder)
    {
        builder.HasKey(model => model.Id);

        builder.ToTable("transferHistory");

        builder.Property(model => model.Amount)
            .IsRequired();

        builder.Property(model => model.CurrencyCode)
            .IsRequired();

        builder.Property(model => model.FromAccountId)
            .IsRequired();

        builder.Property(model => model.ToAccountId)
            .IsRequired();

        builder.HasOne<AccountDbModel>()
            .WithMany()
            .HasForeignKey(model => model.FromAccountId);

        builder.HasOne<AccountDbModel>()
            .WithMany()
            .HasForeignKey(model => model.ToAccountId);
    }
}