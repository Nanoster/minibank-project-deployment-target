using Microsoft.EntityFrameworkCore;
using MiniBank.Data.Domains.Accounts.DbModels;
using MiniBank.Data.Domains.TransferHistories.DbModels;
using MiniBank.Data.Domains.Users.DbModels;

namespace MiniBank.Data;

public class MiniBankContext : DbContext
{
    public MiniBankContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<UserDbModel> Users { get; set; }
    public DbSet<TransferHistoryDbModel> TransferHistories { get; set; }
    public DbSet<AccountDbModel> Accounts { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(MiniBankContext).Assembly);
        base.OnModelCreating(modelBuilder);
    }
}