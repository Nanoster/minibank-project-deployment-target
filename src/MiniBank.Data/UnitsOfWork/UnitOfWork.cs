using MiniBank.Core.UnitsOfWork;

namespace MiniBank.Data.UnitsOfWork;

public class UnitOfWork : IUnitOfWork
{
    private readonly MiniBankContext _context;

    public UnitOfWork(MiniBankContext context)
    {
        _context = context;
    }

    public async Task<int> SaveChanges(CancellationToken token = default)
    {
        return await _context.SaveChangesAsync(token);
    }
}