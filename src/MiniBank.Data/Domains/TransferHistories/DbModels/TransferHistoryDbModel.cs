#pragma warning disable CS8618
namespace MiniBank.Data.Domains.TransferHistories.DbModels;

public class TransferHistoryDbModel
{
    public Guid Id { get; set; }
    public decimal Amount { get; set; }
    public string CurrencyCode { get; set; }
    public Guid FromAccountId { get; set; }
    public Guid ToAccountId { get; set; }
}