using Microsoft.EntityFrameworkCore;
using MiniBank.Core.Domains.Transfers.Interfaces;
using MiniBank.Core.Domains.Transfers.Models;
using MiniBank.Core.Exceptions;
using MiniBank.Data.Domains.TransferHistories.DbModels;

namespace MiniBank.Data.Domains.TransferHistories.Repositories;

public class TransferHistoryRepository : ITransferHistoryRepository
{
    private readonly MiniBankContext _context;

    public TransferHistoryRepository(MiniBankContext context)
    {
        _context = context;
    }

    public async Task<TransferHistory> GetById(Guid historyId, CancellationToken token = default)
    {
        var model = await GetTransferHistoryDbModelById(historyId, false, token);
        return new TransferHistory(model.Id, model.Amount, model.CurrencyCode, model.FromAccountId,
            model.ToAccountId);
    }

    public async Task<IEnumerable<TransferHistory>> GetByUserId(Guid userId, CancellationToken token = default)
    {
        var models = await _context.TransferHistories
            .AsNoTracking()
            .Where(model => model.FromAccountId == userId || model.ToAccountId == userId)
            .ToListAsync(token);

        return models.Select(model =>
            new TransferHistory(model.Id, model.Amount, model.CurrencyCode, model.FromAccountId, model.ToAccountId));
    }

    public Guid Create(TransferHistory history)
    {
        var model = new TransferHistoryDbModel
        {
            Amount = history.Amount,
            CurrencyCode = history.CurrencyCode,
            FromAccountId = history.FromAccountId,
            ToAccountId = history.ToAccountId
        };

        _context.TransferHistories.Add(model);
        return model.Id;
    }

    private async Task<TransferHistoryDbModel> GetTransferHistoryDbModelById(Guid id, bool isTracking,
        CancellationToken token = default)
    {
        var model = isTracking
            ? await _context.TransferHistories.FirstOrDefaultAsync(model => model.Id == id, token)
            : await _context.TransferHistories.AsNoTracking().FirstOrDefaultAsync(model => model.Id == id, token);

        if (model == null)
            throw new ValidationException(
                $"{nameof(TransferHistoryRepository)} didn't find the transfer history (id: {id})",
                "The transfer history with this id was not found"
            );

        return model;
    }
}