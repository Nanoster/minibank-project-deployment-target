using Microsoft.EntityFrameworkCore;
using MiniBank.Core.Domains.Accounts.Interfaces;
using MiniBank.Core.Domains.Accounts.Models;
using MiniBank.Core.Exceptions;
using MiniBank.Data.Domains.Accounts.DbModels;

namespace MiniBank.Data.Domains.Accounts.Repositories;

public class AccountRepository : IAccountRepository
{
    private readonly MiniBankContext _context;

    public AccountRepository(MiniBankContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Account>> GetByUserId(Guid userId, CancellationToken token = default)
    {
        var models = await _context.Accounts
            .AsNoTracking()
            .Where(model => model.UserId == userId && model.IsActive)
            .ToListAsync(token);

        return models
            .Select(model => new Account(
                model.Id,
                model.UserId,
                model.Amount,
                model.CurrencyCode,
                model.IsActive,
                model.OpeningDate,
                model.ClosingDate));
    }

    public Guid Create(Guid userId, string currencyCode)
    {
        var model = new AccountDbModel
        {
            OpeningDate = DateOnly.FromDateTime(DateTime.UtcNow),
            UserId = userId,
            CurrencyCode = currencyCode,
            IsActive = true
        };

        _context.Accounts.Add(model);
        return model.Id;
    }

    public async Task<Account> GetById(Guid accountId, CancellationToken token = default)
    {
        var model = await GetAccountDbModelById(accountId, false, token);
        return new Account(
            model.Id,
            model.UserId,
            model.Amount,
            model.CurrencyCode,
            model.IsActive,
            model.OpeningDate,
            model.ClosingDate);
    }

    public async Task Update(Account account, CancellationToken token = default)
    {
        var oldAccount = await GetAccountDbModelById(account.Id, true, token);
        oldAccount.Amount = account.Amount;
        oldAccount.ClosingDate = account.ClosingDate;
        oldAccount.CurrencyCode = account.CurrencyCode;
        oldAccount.IsActive = account.IsActive;
        oldAccount.OpeningDate = account.OpeningDate;
        oldAccount.UserId = account.UserId;
    }

    public async Task<bool> AccountsByUserIdExist(Guid userId, CancellationToken token = default)
    {
        return await _context.Accounts.AnyAsync(model => model.UserId == userId, token);
    }

    private async Task<AccountDbModel> GetAccountDbModelById(Guid id, bool isTacking, CancellationToken token = default)
    {
        var model = isTacking
            ? await _context.Accounts.FirstOrDefaultAsync(model => model.Id == id, token)
            : await _context.Accounts.AsNoTracking().FirstOrDefaultAsync(model => model.Id == id, token);

        if (model == null)
            throw new ValidationException(
                $"{nameof(AccountRepository)} didn't find the account (id: {id})",
                "The transfer account with this id was not found"
            );

        return model;
    }
}