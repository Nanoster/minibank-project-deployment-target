#pragma warning disable CS8618
namespace MiniBank.Data.Domains.Accounts.DbModels;

public class AccountDbModel
{
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public decimal Amount { get; set; }
    public string CurrencyCode { get; set; }
    public bool IsActive { get; set; }
    public DateOnly OpeningDate { get; set; }
    public DateOnly ClosingDate { get; set; }
}