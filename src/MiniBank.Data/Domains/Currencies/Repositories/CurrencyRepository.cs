using System.Net.Http.Json;
using MiniBank.Core.Domains.Currencies.Interfaces;
using MiniBank.Core.Domains.Currencies.Models;
using MiniBank.Core.Exceptions;
using MiniBank.Data.Domains.Currencies.Models;

namespace MiniBank.Data.Domains.Currencies.Repositories;

public class CurrencyRepository : ICurrencyRepository
{
    private readonly HttpClient _client;

    public CurrencyRepository(HttpClient client)
    {
        _client = client;
    }

    public async Task<decimal> GetCurrencyRateByCode(string currencyCode, CancellationToken token = default)
    {
        var model = await GetCurrencyModel(currencyCode, token);
        return model.Value / model.Nominal;
    }

    public async Task<CurrencyInfo> GetByCode(string currencyCode, CancellationToken token = default)
    {
        var model = await GetCurrencyModel(currencyCode, token);
        return new CurrencyInfo(model.Name, model.CharCode, model.NumCode);
    }

    private async Task<CurrencyModel> GetCurrencyModel(string code, CancellationToken token = default)
    {
        var isNumCode = int.TryParse(code, out var numCode);

        if (string.Equals(code, CbrResponseModel.MainCurrency.CharCode, StringComparison.OrdinalIgnoreCase) ||
            numCode == CbrResponseModel.MainCurrency.NumCode)
            return CbrResponseModel.MainCurrency;

        var response = await _client.GetAsync(string.Empty, token);
        if (!response.IsSuccessStatusCode) throw new HttpRequestException();
        var responseModel = await response.Content.ReadFromJsonAsync<CbrResponseModel>(cancellationToken: token);
        if (responseModel == null) throw new HttpRequestException();

        var model = isNumCode
            ? responseModel.Valute.Values.FirstOrDefault(currencyModel => currencyModel.NumCode == numCode)
            : responseModel.Valute.GetValueOrDefault(code.ToUpper());

        if (model == null)
            throw new ValidationException(
                $"{nameof(CurrencyRepository)} didn't find the currency ({code})",
                $"Currency with this code ({code}) is not found");

        return model;
    }
}