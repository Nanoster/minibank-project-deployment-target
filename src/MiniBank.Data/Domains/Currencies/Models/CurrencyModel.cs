namespace MiniBank.Data.Domains.Currencies.Models;
#pragma warning disable CS8618

public class CurrencyModel
{
    public string Name { get; set; }
    public int NumCode { get; set; }
    public string CharCode { get; set; }
    public int Nominal { get; set; }
    public decimal Value { get; set; }
}