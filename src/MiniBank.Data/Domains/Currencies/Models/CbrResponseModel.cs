using System.Text.Json.Serialization;

namespace MiniBank.Data.Domains.Currencies.Models;
#pragma warning disable CS8618

public class CbrResponseModel
{
    public DateTime Date { get; set; }

    [JsonPropertyName("PreviousURL")] public Uri URL { get; set; }

    public Dictionary<string, CurrencyModel> Valute { get; set; }

    public static CurrencyModel MainCurrency { get; } = new()
    {
        Name = "Ruble",
        Nominal = 1,
        Value = 1,
        CharCode = "RUB",
        NumCode = 643
    };
}