using Microsoft.EntityFrameworkCore;
using MiniBank.Core.Domains.Users.Interfaces;
using MiniBank.Core.Domains.Users.Models;
using MiniBank.Core.Exceptions;
using MiniBank.Data.Domains.Users.DbModels;

namespace MiniBank.Data.Domains.Users.Repositories;

public class UserRepository : IUserRepository
{
    private readonly MiniBankContext _context;
    private readonly IPasswordHashProvider _passwordHashProvider;

    public UserRepository(MiniBankContext context, IPasswordHashProvider passwordHashProvider)
    {
        _context = context;
        _passwordHashProvider = passwordHashProvider;
    }

    public async Task<Guid> Create(User user, string password, CancellationToken token = default)
    {
        if (await _context.Users.AnyAsync(model => model.Email == user.Email, token))
            throw new ValidationException($"User with {user.Email} email already exists");

        var (hash, salt) = _passwordHashProvider.CreateHash(password);
        var model = new UserDbModel
        {
            Id = Guid.NewGuid(),
            Email = user.Email,
            Login = user.Login,
            Password = hash,
            Salt = salt
        };


        _context.Users.Add(model);
        return model.Id;
    }

    public async Task Update(User user, CancellationToken token = default)
    {
        var userDbModel = await GetUserDbModelById(user.Id, true, token);
        userDbModel.Login = user.Login;
        userDbModel.Email = user.Email;
    }

    public async Task DeleteById(Guid userId, CancellationToken token = default)
    {
        var userDbModel = await GetUserDbModelById(userId, false, token);
        _context.Users.Remove(userDbModel);
    }

    public async Task<bool> Exists(Guid userId, CancellationToken token = default)
    {
        return await _context.Users.AnyAsync(model => model.Id == userId, token);
    }

    public async Task<User> GetById(Guid userId, CancellationToken token = default)
    {
        var user = await GetUserDbModelById(userId, false, token);
        return new User(user.Id, user.Login, user.Email);
    }

    private async Task<UserDbModel> GetUserDbModelById(Guid id, bool isTracking, CancellationToken token = default)
    {
        var userDbModel = isTracking
            ? await _context.Users.FirstOrDefaultAsync(model => model.Id == id, token)
            : await _context.Users.AsNoTracking().FirstOrDefaultAsync(model => model.Id == id, token);

        if (userDbModel == null)
            throw new ValidationException(
                $"{nameof(UserRepository)} didn't find the user (id: {id})",
                "The user with this id was not found");

        return userDbModel;
    }
}