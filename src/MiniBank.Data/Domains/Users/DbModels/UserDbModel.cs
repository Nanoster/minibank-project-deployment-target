#pragma warning disable CS8618
namespace MiniBank.Data.Domains.Users.DbModels;

public class UserDbModel
{
    public Guid Id { get; set; }
    public string Login { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public byte[] Salt { get; set; }
}