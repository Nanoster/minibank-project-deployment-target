﻿using Microsoft.EntityFrameworkCore;
using MiniBank.Migrations;

if (args.FirstOrDefault()?.Trim() != "migrate") return;

Console.WriteLine(@"Build...");

var factory = new Factory();
var context = factory.CreateDbContext(args);

Console.WriteLine(@"Build succeed!");

context.Database.Migrate();

Console.WriteLine($@"{DateTime.UtcNow} - Migrations successfully applied");