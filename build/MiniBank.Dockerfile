FROM mcr.microsoft.com/dotnet/sdk:6.0 AS src
WORKDIR /src
COPY ./src .
RUN dotnet build MiniBank.Web -c Release -r linux-x64
RUN dotnet test ./Tests/MiniBank.Core.Tests --no-build
RUN dotnet publish MiniBank.Web -c Release -r linux-x64 --no-build -o /dist
RUN dotnet publish MiniBank.Migrations -c Release -r linux-x64 -o /migr

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS final
WORKDIR /app
COPY --from=src /dist ./api
COPY --from=src /migr ./migr
COPY ./build/scripts ./scripts
ENV ASPNETCORE_URLS=http://+:5000
ENV ASPNETCORE_ENVIRONMENT=Development
EXPOSE 5000
ENTRYPOINT ["sh", "./scripts/MiniBank.sh"]
